export class TypeTest {
    constructor(
        public id?: number,
        public code?: string,
        public nom?: string,
    ) {
    }
}
